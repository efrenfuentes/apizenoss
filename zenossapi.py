# Zenoss-4.x JSON API

import json
import urllib
import urllib2

ROUTERS = { 'MessagingRouter': 'messaging',
            'EventsRouter': 'evconsole',
            'ProcessRouter': 'process',
            'ServiceRouter': 'service',
            'DeviceRouter': 'device',
            'NetworkRouter': 'network',
            'TemplateRouter': 'template',
            'DetailNavRouter': 'detailnav',
            'ReportRouter': 'report',
            'MibRouter': 'mib',
            'ZenPackRouter': 'zenpack' }

class ZenossAPI(object):
    def __init__(self, instance, username, password):
        """
        Initialize the API connection, log in, and store authentication cookie
        """
        # Store instance for use on next API requests
        self.instance = instance

        # Use the HTTPCookieProcessor as urllib2 does not save cookies by
        # default
        self.urlOpener = urllib2.build_opener(urllib2.HTTPCookieProcessor())
        self.requestsCount = 1

        # Contruct POST params and submit login.
        loginParams = urllib.urlencode(dict(
                        __ac_name = username,
                        __ac_password = password,
                        submitted = 'true',
                        came_from = '{0}/zport/dmd'.format(self.instance)))
        self.urlOpener.open(self.instance + '/zport/acl_users/cookieAuthHelper/login',
                            loginParams)

    def _do_request(self, router, method, data=[]):
        if router not in ROUTERS:
            raise Exception('Router "{0}" not available.'.format(router))

        # Contruct a standard URL request for API calls
        request = urllib2.Request('{0}/zport/dmd/{1}_router'.format(self.instance, ROUTERS[router]))

        # NOTE: Content-type MUST be set to 'application/json' for these 
        # requests
        request.add_header('Content-type', 'application/json; charset=utf-8')

        # Convert the request parameters into JSON
        requestData = json.dumps([dict(
                      action=router,
                      method=method,
                      data=data,
                      type='rpc',
                      tid=self.requestsCount)])
        
        # Increment the request count ('tid'). More important if sending
        # multiple calls in a single request
        self.requestsCount += 1

        # Submit the request and convert the returned JSON to objects
        return json.loads(self.urlOpener.open(request, requestData).read())

    def getDevices(self, uid='/zport/dmd/Devices', start=0, params={}, limit=50, sort='name', dir='ASC', keys=None):
        """
        
        Retrieves a list of devices. This method supports pagination.

        @type  uid: string
        @param uid: Unique identifier of the organizer to get devices from
        @type  start: integer
        @param start: (optional) Offset to return the results from; used in
                      pagination (default: 0)
        @type  params: dictionary
        @param params: (optional) Key-value pair of filters for this search.
                       Can be one of the following: name, ipAddress,
                       deviceClass, or productionState (default: None)
        @type  limit: integer
        @param limit: (optional) Number of items to return; used in pagination
                      (default: 50)
        @type  sort: string
        @param sort: (optional) Key on which to sort the return results (default:
                     'name')
        @type  dir: string
        @param dir: (optional) Sort order; can be either 'ASC' or 'DESC'
                    (default: 'ASC')
        @rtype:   DirectResponse
        @return:  B{Properties}:
             - devices: (list) Dictionaries of device properties
             - totalCount: (integer) Number of devices returned
             - hash: (string) Hashcheck of the current device state (to check
             whether devices have changed since last query)
        
        """
        data = locals().copy()
        del data['self']

        return self._do_request('DeviceRouter', 'getDevices', [data])['result']['devices']
    
    def getGroups(self):
        """
        
        Get a list of all groups.

        @rtype:   DirectResponse
        @return:  B{Properties}:
             - systems: ([dictionary]) List of groups
             - totalCount: (integer) Total number of groups
        
        """
        return self._do_request('DeviceRouter', 'getGroups', data=[])['result']['groups']

    def getEvents(self, uid):
        data = locals().copy()
        del data['self']
        
        return self._do_request('DeviceRouter', 'getEvents', [data])

    def getLocations(self):
        """
        
        Get a list of all locations.

        @rtype:   DirectResponse
        @return:  B{Properties}:
             - systems: ([dictionary]) List of locations
             - totalCount: (integer) Total number of locations
        
        """
        return self._do_request('DeviceRouter', 'getLocations', data=[])['result']['locations']

    def queryEvents(self, limit=0, start=0, sort='lastTime', dir='desc', params=None, archive=False, uid=None, detailFormat=False):
        """
        
        Query for events.

        @type  limit: integer
        @param limit: (optional) Max index of events to retrieve (default: 0)
        @type  start: integer
        @param start: (optional) Min index of events to retrieve (default: 0)
        @type  sort: string
        @param sort: (optional) Key on which to sort the return results (default:
                     'lastTime')
        @type  dir: string
        @param dir: (optional) Sort order; can be either 'ASC' or 'DESC'
                    (default: 'DESC')
        @type  params: dictionary
        @param params: (optional) Key-value pair of filters for this search.
                       (default: None)
        @type  archive: boolean
        @param archive: (optional) True to search the event history table instead
                        of active events (default: False)
        @type  uid: string
        @param uid: (optional) Context for the query (default: None)
        @rtype:   dictionary
        @return:  B{Properties}:
           - events: ([dictionary]) List of objects representing events
           - totalCount: (integer) Total count of events returned
           - asof: (float) Current time
        
        """
        data = locals().copy()
        del data['self']

        return self._do_request('EventsRouter', 'query', [data])['result']